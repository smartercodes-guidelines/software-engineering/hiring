# Contributing
* [Explode the Full Stack Engineering Role in Smarter.Codes into multiple roles](#1) (Recruitment led by @abhishekd-sb)
  - UI Developer
    - [Draft job description for UI Developer](#3)
    - [Draft Roadmap for UI Developer](#5)
    - [Ideate the recruitment test for UI Developer](#6)
  - Full Stack Engineer
    - [Draft job description for Full Stack Engineer](#4)
    - [Draft Roadmap for Full Stack Engineer](#2)
    - Ideate the recruitment test for Full Stack Engineer
  - Backend Engineer
    - Draft job description for Backend Engineer
    - Draft Roadmap for Backend Engineer
    - Ideate the recruitment test for Backend Engineer

* Blockchain Engineering team (Recruitment led by @ranadip-s)
  - Full Stack Engineer (heavy in frontnend framework) with Blockchain Experience (web3js)
    - Job Description
    - Roadmap
    - Recruitment test
  - Smart Contract Engineer
    - Job Description
    - Roadmap
    - Recruitment test

# Inspirations
* [Hiring without whiteboards](https://github.com/poteto/hiring-without-whiteboards)
* [Smart and Gets Things Done](https://learning.oreilly.com/library/view/smart-and-gets/9781590598382/)[<sup>Get Paid Access</sup>](https://gitlab.com/smarter-codes/guidelines/about-guidelines/-/blob/master/paid-learning-material.md)

# See Also
[Home of Hiring Guidelines at Smarter.Codes](https://gitlab.com/smarter-codes/guidelines/Hiring)
